﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace ProjectStructureWebAPI.Models
{
    public class Task
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        [Required]
        public string Name { get; set; }

        [JsonProperty("description")]
        [MaxLength(250)]
        public string Description { get; set; }

        [JsonProperty("created_at")]
        [DataType(DataType.DateTime)]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("finished_at")]
        [DataType(DataType.DateTime)]
        public DateTime FinishedAt { get; set; }

        [JsonProperty("state")]
        public TaskState State { get; set; }

        [JsonProperty("project_id")]
        public int ProjectId { get; set; }

        [JsonProperty("performer_id")]
        public int PerformerId { get; set; }

        [JsonProperty("reviewer_id")]
        public int? ReviewerId { get; set; }
    }
}
