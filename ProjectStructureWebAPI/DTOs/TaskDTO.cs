﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ProjectStructureWebAPI.Models;

namespace ProjectStructureWebAPI.DTOs
{
    public class TaskDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }
        [JsonProperty("finished_at")]
        public DateTime FinishedAt { get; set; }
        [JsonProperty("state")]
        public TaskState State { get; set; }
        [JsonProperty("project_id")]
        public int ProjectId { get; set; }
        [JsonProperty("performer_id")]
        public int PerformerId { get; set; }

    }
}
