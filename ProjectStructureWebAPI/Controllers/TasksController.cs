﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectStructureWebAPI.Services;
using ProjectStructureWebAPI.DTOs;

namespace ProjectStructureWebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly TaskService _taskService;

        public TasksController(TaskService taskService)
        {
            _taskService = taskService;
        }
        [HttpGet]
        public IEnumerable<TaskDTO> GetTasks()
        {
            return _taskService.Get();
        }
        [HttpGet("{id}")]
        public TaskDTO Get([FromBody] int id)
        {
            return _taskService.GetSingle(id);
        }
        [HttpPost]
        public void Add([FromBody] TaskDTO taskDto)
        {
            _taskService.Add(taskDto);
        }
        [HttpDelete("{id}")]
        public void Delete([FromBody] int id)
        {
            _taskService.Delete(id);
        }
        [HttpPut("{id}")]
        public void Update(int id, [FromBody] TaskDTO taskDto)
        {
            _taskService.Update(id, taskDto);
        }

    }
}
