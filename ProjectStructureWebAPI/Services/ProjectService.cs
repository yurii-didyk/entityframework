﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructureWebAPI.DTOs;
using ProjectStructureWebAPI.Models;
using ProjectStructureWebAPI.Context;
using AutoMapper;

namespace ProjectStructureWebAPI.Services
{
    public class ProjectService
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly QueueService _queueService;

        public ProjectService(DataContext context,
            IMapper mapper, QueueService queueService)
        {
            _context = context;
            _mapper = mapper;
            _queueService = queueService;
        }

        public void Add(ProjectDTO projectDto)
        {
            _queueService.Post("Project creation was triggered");
            var project = _mapper.Map<Project>(projectDto);
            _context.Projects.Add(project);
            _context.SaveChanges();
        }
        public void Delete(int id)
        {
            _queueService.Post("Project deletion was triggered");
            _context.Projects.Remove(_context.Projects.FirstOrDefault(p => p.Id == id));
            _context.SaveChanges();
        }
        public void Update(int id, ProjectDTO projectDto)
        {
            _queueService.Post("Project updating was triggered");
            var project = _mapper.Map<Project>(projectDto);
            var projectToUpdate = _context.Projects.FirstOrDefault(p => p.Id == id);

            projectToUpdate.Name = project.Name;
            projectToUpdate.Description = project.Description;
            projectToUpdate.Deadline = project.Deadline;
            projectToUpdate.TeamId = project.TeamId;

            _context.Projects.Update(projectToUpdate);

            _context.SaveChanges();
        }
        public ProjectDTO GetSingle(int id)
        {
            _queueService.Post("Loading single project was triggered");
            var project = _context.Projects.FirstOrDefault(p => p.Id == id);
            return _mapper.Map<ProjectDTO>(project);
        }
        public IEnumerable<ProjectDTO> Get()
        {
            _queueService.Post("Loading all projects was triggered");
            var projects = _context.Projects;
            return _mapper.Map<IEnumerable<ProjectDTO>>(projects);
        }
    }
}
