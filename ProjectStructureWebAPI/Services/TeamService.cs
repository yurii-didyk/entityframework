﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructureWebAPI.DTOs;
using ProjectStructureWebAPI.Models;
using AutoMapper;
using ProjectStructureWebAPI.Context;

namespace ProjectStructureWebAPI.Services
{
    public class TeamService
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly QueueService _queueService;

        public TeamService(DataContext context,
            IMapper mapper, QueueService queueService)
        {
            _context = context;
            _mapper = mapper;
            _queueService = queueService;
        }

        public void Add(TeamDTO teamDto)
        {
            _queueService.Post("Team creating was triggered");
            var team = _mapper.Map<Team>(teamDto);
            _context.Teams.Add(team);
            _context.SaveChanges();
        }
        public void Delete(int id)
        {
            _queueService.Post("Team deletion was triggered");
            _context.Teams.Remove(_context.Teams.FirstOrDefault(t => t.Id == id));
            _context.SaveChanges();
        }
        public void Update(int id, TeamDTO teamDto)
        {
            _queueService.Post("Team updating was triggered");
            var team = _mapper.Map<Team>(teamDto);
            var teamToUpdate = _context.Teams.FirstOrDefault(t => t.Id == id);

            teamToUpdate.Name = team.Name;

            _context.Teams.Update(teamToUpdate);
            _context.SaveChanges();
        }
        public TeamDTO GetSingle(int id)
        {
            _queueService.Post("Loading single team was triggered");
            var team = _context.Teams.FirstOrDefault(t => t.Id == id);
            return _mapper.Map<TeamDTO>(team);
        }
        public IEnumerable<TeamDTO> Get()
        {
            _queueService.Post("Loading all teams was triggered");
            var teams = _context.Teams;
            return _mapper.Map<IEnumerable<TeamDTO>>(teams);
        }
    }
}
