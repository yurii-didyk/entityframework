﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructureWebAPI.DTOs;
using ProjectStructureWebAPI.Models;
using AutoMapper;
using ProjectStructureWebAPI.Context;

namespace ProjectStructureWebAPI.Services
{
    public class UserService
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly QueueService _queueService;


        public UserService(DataContext context,
            IMapper mapper, QueueService queueService)
        {
            _context = context;
            _mapper = mapper;
            _queueService = queueService;
        }

        public void Add(UserDTO teamDto)
        {
            _queueService.Post("User creation was triggered");
            var user = _mapper.Map<User>(teamDto);
            _context.Users.Add(user);
            _context.SaveChanges();
        }
        public void Delete(int id)
        {
            _queueService.Post("User deletion was triggered");
            _context.Users.Remove(_context.Users.FirstOrDefault(u => u.Id == id));
            _context.SaveChanges();
        }
        public void Update(int id, UserDTO teamDto)
        {
            _queueService.Post("User updating was triggered");
            var user = _mapper.Map<User>(teamDto);
            var userToUpdate = _context.Users.FirstOrDefault(u => u.Id == id);

            userToUpdate.FirstName = user.FirstName;
            userToUpdate.LastName = user.LastName;
            userToUpdate.TeamId = user.TeamId;
            userToUpdate.Email = user.Email;
            userToUpdate.Birthday = user.Birthday;

            _context.Users.Update(userToUpdate);
            _context.SaveChanges();
        }
        public UserDTO GetSingle(int id)
        {
            _queueService.Post("Loading single user was triggered");
            var user = _context.Users.FirstOrDefault(u => u.Id == id);
            return _mapper.Map<UserDTO>(user);
        }
        public IEnumerable<UserDTO> Get()
        {
            _queueService.Post("Loading all users was triggered");
            var users = _context.Users;
            return _mapper.Map<IEnumerable<UserDTO>>(users);
        }
    }
}