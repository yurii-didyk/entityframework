﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructureWebAPI.DTOs;
using AutoMapper;
using ProjectStructureWebAPI.Context;

namespace ProjectStructureWebAPI.Services
{
    public class TaskService
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly QueueService _queueService;

        public TaskService(DataContext context,
            IMapper mapper, QueueService queueService)
        {
            _context = context;
            _mapper = mapper;
            _queueService = queueService;
        }

        public void Add(TaskDTO taskDto)
        {
            _queueService.Post("Task creation was triggered");
            var task = _mapper.Map<Models.Task>(taskDto);
            _context.Tasks.Add(task);
            _context.SaveChanges();
        }
        public void Delete(int id)
        {
            _queueService.Post("Task deletion was triggered");
            _context.Tasks.Remove(_context.Tasks.FirstOrDefault(t => t.Id == id));
            _context.SaveChanges();
        }
        public void Update(int id, TaskDTO taskDto)
        {
            _queueService.Post("Task updating was triggered");
            var task = _mapper.Map<Models.Task>(taskDto);
            var taskToUpdate = _context.Tasks.FirstOrDefault(t => t.Id == id);

            taskToUpdate.Name = task.Name;
            taskToUpdate.Description = task.Description;
            taskToUpdate.FinishedAt = task.FinishedAt;
            taskToUpdate.PerformerId = task.PerformerId;
            taskToUpdate.ProjectId = task.ProjectId;
            taskToUpdate.State = task.State;

            _context.Tasks.Update(taskToUpdate);
            _context.SaveChanges();
        }
        public TaskDTO GetSingle(int id)
        {
            _queueService.Post("Loading single task was triggered");
            var task = _context.Tasks.FirstOrDefault(t => t.Id == id);
            return _mapper.Map<TaskDTO>(task);
        }
        public IEnumerable<TaskDTO> Get()
        {
            _queueService.Post("Loading all tasks was triggered");
            var tasks = _context.Tasks;
            return _mapper.Map<IEnumerable<TaskDTO>>(tasks);
        }

    }
}
