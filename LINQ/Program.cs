﻿using System;
using System.Linq;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using LINQ.Models;
using Microsoft.AspNetCore.SignalR.Client;
using System.Net.Http.Headers;
using ProjectStructureWebAPI;

namespace LINQ
{
    class Program
    {
         static void Main(string[] args)
        {
            try
            {
                SignalRRun().Wait();
                SignalRLogsRun().Wait();

            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Menu menu = new Menu();
            menu.Run();
        }

        static async System.Threading.Tasks.Task SignalRRun()
        {
            var config = new AppConfiguration();
            var connection = new HubConnectionBuilder()
               .WithUrl($"{config.Server}messages")
               .Build();
            connection.On<string>("SendResponse", (message) =>
            {
                Console.WriteLine($"{message}");
            });
            await connection.StartAsync();
        }

        static async System.Threading.Tasks.Task SignalRLogsRun()
        {
            var config = new AppConfiguration();
            var connection = new HubConnectionBuilder()
               .WithUrl($"{config.Server}logs")
               .Build();
            connection.On<IEnumerable<MessageModel>>("LogsReceived", (messages) =>
            {
                foreach(var item in messages)
                {
                    Console.WriteLine($"Message: {item.Message}\nCreatedAt: {item.CreatedAt}");
                }
            });
            await connection.StartAsync();
        }

    }
}
