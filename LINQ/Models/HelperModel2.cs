﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LINQ.Models
{
    class HelperModel2
    {
        public Project Project { get; set; }
        public Task LongestTaskByName { get; set; }
        public Task LongestTaskByDescription { get; set; }
        public int UsersCount { get; set; }

        public override string ToString()
        {
            string result = ($"Project: {Project.ToString()}\n\nLongest task by name: {LongestTaskByName.ToString()}\n\nLongest task by description: {LongestTaskByDescription.ToString()}\n\nUsers count: {UsersCount}");
            return result;
        }
    }
}
